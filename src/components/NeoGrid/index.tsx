import { AgGridReact } from "ag-grid-react";
import data from "../../near-earth-asteroids.json";
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-alpine.css";
import { IAsteroidData } from "../../interfaces";
import "./styles.css";
import { columnVisitor } from "../../hooks/use-neogrid/utils";
import useNeoGrid from "../../hooks/use-neogrid";

const NeoGrid = (): JSX.Element => {
  const {
    clearFiltersAndSorters,
    onGridReady,
    defaultColDef,
    columnDefs,
    gridRef,
  } = useNeoGrid();

  return (
    <div className="ag-theme-alpine" style={{ height: 900, width: 1920 }}>
      <div className="title-container">
        <h1>Near-Earth Object Overview</h1>
        <button onClick={clearFiltersAndSorters}>
          Clear Filters and Sorters
        </button>
      </div>
      <AgGridReact<IAsteroidData>
        ref={gridRef}
        rowData={data as any}
        columnDefs={columnDefs.map(columnVisitor)}
        rowGroupPanelShow={"always"}
        rowSelection="multiple"
        onGridReady={onGridReady}
        defaultColDef={defaultColDef}
      />
    </div>
  );
};

export default NeoGrid;
