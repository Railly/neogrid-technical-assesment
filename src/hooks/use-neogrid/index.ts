import type { AgGridReact } from "ag-grid-react";
import type { ColDef } from "ag-grid-community";
import { useRef, useState, useCallback, useEffect, useMemo } from "react";
import { IAsteroidData } from "../../interfaces";
import { COLUMN_DEFS } from "../../components/NeoGrid/constants";
import { columnVisitor, formatJSONToExcelCells } from "./utils";
import data from "../../near-earth-asteroids.json";

const useNeoGrid = () => {
  const gridRef = useRef<AgGridReact<IAsteroidData>>(null);
  const [columnDefs, setColumnDefs] = useState<ColDef[]>([]);

  const clearFiltersAndSorters = useCallback(() => {
    gridRef.current!.api.setFilterModel(null);
    gridRef.current!.columnApi.applyColumnState({
      defaultState: { sort: null },
    });
  }, []);

  const defaultColDef = useMemo<ColDef>(() => {
    return {
      filter: true,
      sortable: true,
    };
  }, []);

  const copyToClipboard = useCallback(
    (event: KeyboardEvent) => {
      if (event.ctrlKey && event.key === "c") {
        const selectedNodesData = gridRef
          .current!.api.getSelectedNodes()
          .map((node) => node.data);

        const excelCells = formatJSONToExcelCells(selectedNodesData);

        navigator.clipboard.writeText(excelCells);
      }
    },
    [gridRef]
  );

  useEffect(() => {
    window.addEventListener("keydown", copyToClipboard);

    return () => window.removeEventListener("keydown", copyToClipboard);
  }, [copyToClipboard]);

  const onGridReady = useCallback(() => {
    setColumnDefs(COLUMN_DEFS.map(columnVisitor));
  }, []);

  return {
    gridRef,
    columnDefs,
    defaultColDef,
    onGridReady,
    rowData: data,
    clearFiltersAndSorters,
  };
};

export default useNeoGrid;
