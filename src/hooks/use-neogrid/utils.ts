import type { ColDef } from "ag-grid-community";
import { IAsteroidData } from "../../interfaces";

const withFilterAndComparator = (column: ColDef<IAsteroidData>) => {
  switch (column.type) {
    case "number":
      column.comparator = (valueA: any, valueB: any) => {
        return valueA - valueB;
      };
      column.filter = "agNumberColumnFilter";
      break;
    case "date":
      column.comparator = (valueA: any, valueB: any) => {
        return new Date(valueA).getTime() - new Date(valueB).getTime();
      };
      column.filter = "agTextColumnFilter";
      break;
    default:
      column.filter = "agTextColumnFilter";
      break;
  }
  return column;
};

const parseColumn = (columnName: keyof IAsteroidData) => {
  const parser = {
    pha: (value: any) => {
      switch (value) {
        case "Y":
          return "Yes";
        case "N":
          return "No";
        default:
          return "";
      }
    },
    discovery_date: (value: any) => {
      return new Date(value).toLocaleDateString(undefined, {
        year: "numeric",
        month: "long",
        day: "numeric",
      });
    },
  };
  if (columnName in parser) {
    return parser[columnName as keyof typeof parser];
  }
  return (value: any) => value;
};

export const columnVisitor = (column: ColDef<IAsteroidData>) => {
  switch (column.field) {
    case "pha":
      column.cellRenderer = (params: any) => parseColumn("pha")(params.value);
      break;
    case "discovery_date":
      column.valueFormatter = (params) =>
        parseColumn("discovery_date")(params.value);
      break;
    default:
      break;
  }
  return withFilterAndComparator(column);
};

export const formatJSONToExcelCells = (json: (IAsteroidData | undefined)[]) => {
  const jsonFormatted = json.map((row) => {
    if (row) {
      const formattedRow = Object.entries(row).reduce((acc, [key, value]) => {
        const assertedKey = key as keyof IAsteroidData;
        acc[assertedKey] = parseColumn(assertedKey)(value);
        return acc;
      }, {} as any);
      return formattedRow;
    }
    return row;
  });

  const rows = jsonFormatted.map((row) => Object.values(row as IAsteroidData));

  return `${rows.map((row) => row.join("\t")).join("\r")}`;
};
